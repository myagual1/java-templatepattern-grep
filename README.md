CS442 Design Patterns
Fall 2016
ASSIGNMENT 4 README FILE

Due Date: Wednesday, November 23, 2016
Submission Date: Sunday, November 27, 2016
Grace Period Used This Project: 3
Grace Period Remaining: 0
Author(s): Mariuxi Yagual
e-mail(s): myagual1@binghamton.edu


PURPOSE:

[
 	Create the wc command with Visitor, Prototype, and Observer patterns. I used the AVL tree to store the text because 
 	it's insertion time complexity is O(ln(n)) for both best and worst case which is better than the worst case time complexity of a 
 	binary tree. The tree is always balanced which makes it easier to find, insert and so on. We will not have a case in which it 
 	looks like a link list. 
]

PERCENT COMPLETE:

[
  I believe I have completed 100% of this project in all its entirety 
]

PARTS THAT ARE NOT COMPLETE:

[
  All parts are complete. I did not include a getters/setters 
  because there was no purpose for such for a couple of attributes.
]

BUGS:

[
 None
]

FILES:

[
  Included with this project are 15 files
  README, the text file you are presently reading
]

SAMPLE OUTPUT:

[
  Mariuxis-MacBook-Air:wordCount mariuxiyagual$ ant -Dargs1=wordCount/input.txt -Dargs2=wordCount/output.txt -Dargs3=3 run
Buildfile: /Users/mariuxiyagual/Desktop/cs442/yagual_mariuxi_assign4/wordCount/build.xml

jar:
      [jar] Building jar: /Users/mariuxiyagual/Desktop/cs442/yagual_mariuxi_assign4/wordCount/BUILD/jar/wordCount.jar

run:
     [java] total_time value is 16

BUILD SUCCESSFUL
Total time: 0 seconds
]

TO COMPILE:

[
  Extract the files and then in the terminal type "ant"
]

TO RUN:

[
  In the terminal, please run as: "ant -Dargs1=wordCount/input.txt -Dargs2=wordCount/output.txt -Dargs3=3 run"
  The values after the equal sign are the arguments, please enter your own 
]

EXTRA CREDIT:

[
  N/A
]


BIBLIOGRAPHY:

This serves as evidence that we are in no way intending Academic Dishonesty.

[
	The professor told me I could use the code for just the data structure found online. I added more functions to the avltree class,
	I put my name on the functions I made, for instance I made the clone and accept methods in the avl tree class. 
  * //https://users.cs.fiu.edu/~weiss/dsaajava2/code/AvlTree.java


]

ACKNOWLEDGEMENT:

[
  During the coding process no classmates were consulted.

]