package wordCount.driver;
import wordCount.visitors.*;
import wordCount.test.*;
import java.io.BufferedReader;
import wordCount.dsForStrings.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Integer;


public class Driver {

	public static void main(String[] args){
			if(args.length < 0){
				System.out.println("Please specify input and output files");
				System.exit(0);
			} 

		int NUM_ITERATIONS = Integer.parseInt(args[2]);
		long startTime = System.currentTimeMillis();
		
		AvlTree a = null;
		
		for(int i = 0; i <= NUM_ITERATIONS; i++){
			//declare/instantiate the data structure and visitors
			a = new AvlTree();
			VisitorInterface vi1 = new PopulateVisitor(args[0]);
			VisitorInterface vi2 = new WordCountVisitor(args[1]);
			//code to visit with the PopulateVisitor
			a.accept(vi1);
			//code to visit with the WordCountVisitor
			a.accept(vi2);
		}
		
		long finishTime = System.currentTimeMillis();
		long total_time = (finishTime-startTime)/NUM_ITERATIONS;
		//Write the total_time value to stdout
		System.out.println("total_time value is " + total_time);
		
		VisitorInterface vi3 = new CloneObserveVisitor();
		a.accept(vi3);
		VisitorInterface vi4 = new TestingVisitor();
		a.accept(vi4);
		a.accept(vi3);
	}
}