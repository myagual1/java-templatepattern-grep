package wordCount.dsForStrings;

import java.lang.String;
import wordCount.visitors.*;

public class AvlNode{
	String element;      // The data in the node
    public AvlNode left;         // Left child
    public AvlNode right;        // Right child
    int	height;       // Height
    //keeps tracks of instances of the same word 
    private int repcount;
    
    // Constructors
    AvlNode( String theElement )
    {
        this( theElement, null, null );
    }

    AvlNode( String theElement, AvlNode lt, AvlNode rt )
    {
        element  = theElement;
        left     = lt;
        right    = rt;
        height   = 0;
        repcount = 1;
    }  
    
    public String getelement(){
    	return element;
    }
    
    public void setelement(String s){
    	element = s;
    }
    
    void increaseRepcount(){
    	repcount++;
    }
    
    public int getRepcount(){
    	return repcount;
    }
    
    public Object clone(){
    	try{
    		return super.clone();
    	}catch(CloneNotSupportedException c){
    		c.printStackTrace();
			System.exit(0);
    	}
    	return null;
    }
    
    
}