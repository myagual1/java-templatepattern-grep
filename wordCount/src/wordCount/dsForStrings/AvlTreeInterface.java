package wordCount.dsForStrings;
import wordCount.visitors.*;

public interface AvlTreeInterface extends Cloneable{
	public void insert( String x );
	public void remove( String x );
	public String findMin( );
	public String findMax( );
	public AvlNode contains( String x );
	public void makeEmpty( );
	public boolean isEmpty( );
	public void printTree(int p);
	public int getTotalWords();
	public int getUniques();
	public int getChars();
	public void accept(VisitorInterface v);
	public AvlTree clone();
	 
}