package wordCount.observer;
import wordCount.dsForStrings.*;

public class ConcreteObserver implements Observer{
	private Subject subject;
	private AvlTree clone;
	
	public ConcreteObserver(Subject s, AvlTree c){
		subject = s;
		clone = c;
		subject.registerObserver(this);
	}
	public void update(AvlTree org){
		if(org.getroot() == null){
			System.out.println("original tree is empty");
			return;
		}
		else if(clone.getroot() == null){
			System.out.println("cloned tree is empty");
			return;
		}
		update(org.getroot(), clone.getroot());
	}
	
	public AvlTree getTree(){
		return clone;
	}
	
	private void update(AvlNode a, AvlNode b){
		if (a==null && b==null)return;
		else if (a!= null && b!= null)
	    {
			if(a.getelement() == b.getelement()){
				update(a.left, b.left);
	            update(a.right, b.right);
	        }
			else{
				b = (AvlNode)a.clone();
			}
	    }
	}
	
}
