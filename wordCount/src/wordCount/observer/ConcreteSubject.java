package wordCount.observer;
import wordCount.dsForStrings.*;

public class ConcreteSubject implements Subject{
	private Observer observer;
	private AvlTree a;
	
	public ConcreteSubject(AvlTree av){
		a = av;
	}
	
	public void registerObserver(Observer o){
		observer = o;
	}
	
	public void removeObserver(Observer o){
		observer = null;
	}
	
	public void notifyObserver(){
		observer.update(a);
	}
	
	private void nodeChanged(){
		notifyObserver();
	}
	
	public void checkTree(){
		if(!checkforChange()){
			//System.out.println("trees not equual");
			nodeChanged();
		}
		else{
			//System.out.println("trees are equal");
			return;
		}
	}
	
	public boolean checkforChange(){
		AvlTree tmp = observer.getTree();
		if(a.getroot() == null){
			System.out.println("original tree is empty");
			return false;
		}
		else if(tmp.getroot() == null){
			System.out.println("cloned tree is empty");
			return false;
		}
		return checkforChange(a.getroot(), tmp.getroot());
	}
	
	private boolean checkforChange(AvlNode a, AvlNode b){
		if (a==null && b==null){
	        return true;
		}
		else if (a!= null && b!= null)
	    {
			if(a.getelement() == b.getelement()){
				checkforChange(a.left, b.left);
	            checkforChange(a.right, b.right);
	        }
			else{
				return false;
			}
	    }
	    return true;
	}
}