package wordCount.observer;
import wordCount.dsForStrings.*;

public interface Observer{
	public void update(AvlTree org);
	public AvlTree getTree();
}

