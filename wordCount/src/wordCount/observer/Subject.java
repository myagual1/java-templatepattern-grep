package wordCount.observer;

/**
 * Subject interface
 * @author mariuxiyagual
 *
 */
public interface Subject{
	public void registerObserver(Observer o);
	public void removeObserver(Observer o);
	public void notifyObserver();
	public void checkTree();
}