package wordCount.test;
import wordCount.visitors.VisitorInterface;
import wordCount.dsForStrings.*;
import wordCount.util.*;

/**
 * Class used to  write to different files the String values in the nodes using an 
 * inorder traversal of the trees
 * @author mariuxiyagual
 *
 */

public class PrintTest{
	/**
	 * Prints trees to the two files that contain the updates
	 * @param AvlTree
	 * @param AvlTree
	 */
	public void printtests(AvlTree a, AvlTree b){
		print(a, "wordCount/src/wordCount/test/orig.txt");
		print(b, "wordCount/src/wordCount/test/clone.txt");
	}
	
	/**
	 * Prints inorder traversal of a tree to file
	 * @param AvlTree
	 * @param String
	 */
	public void print(AvlTree a, String st){
		FileProcessor fp = new FileProcessor();
		fp.openWriter(st);
		if(a.isEmpty( )){
			fp.writeLine( "Empty tree" );
			return;
        }
		else{
            printTree(a.getroot(), fp);
        }
		fp.closeWriter();
	}
    
    /**
     * Internal method to print a subtree in sorted order.
     * @param t the node that roots the tree.
     */
    private void printTree( AvlNode t, FileProcessor fp)
    {
        if( t != null )
        {
            printTree(t.left, fp);
            fp.writeLine( t.getelement());
            printTree( t.right, fp);
        }
    }
}