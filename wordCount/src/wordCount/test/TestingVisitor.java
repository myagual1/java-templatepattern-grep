package wordCount.test;
import wordCount.visitors.VisitorInterface;
import wordCount.dsForStrings.*;

/**
 *  visitor class that updates the integer value in the nodes of the original tree.
 * @author mariuxiyagual
 *
 */
public class TestingVisitor implements VisitorInterface{
	public void visit(AvlTree a){
		PrintTest pt = new PrintTest();
		pt.print(a, "wordCount/src/wordCount/test/inputtree.txt");
		
		AvlNode an = a.contains("the");
		if(an != null){
			an.setelement("egg");
		}
		AvlNode an1 = a.contains("a");
		if(an1 != null){
			an1.setelement("bacon");
		}
		AvlNode an2 = a.contains("with");
		if(an2 != null){
			an2.setelement("syrup");
		}
		AvlNode an3 = a.contains("you");
		if(an3 != null){
			an3.setelement("flower");
		}
		AvlNode an4 = a.contains("but");
		if(an4 != null){
			an4.setelement("KLOOM");
		}
	}
}