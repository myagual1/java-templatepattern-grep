package wordCount.visitors;
import wordCount.dsForStrings.*;
import wordCount.observer.*;
import wordCount.test.*;

/**
 * A third visitor (Clone-and-Observe-Visitor) that clones the data structure so that it can be used as a backup
 * @author mariuxiyagual
 *
 */

public class CloneObserveVisitor implements VisitorInterface{
	AvlTree copyx;
	Subject s;
	Observer o;
	
	public void visit(AvlTree a){
		if(copyx == null){
			copyx = (AvlTree)a.clone();
			s = new ConcreteSubject(a);
			o = new ConcreteObserver(s, copyx);
		}
		else{
			s.checkTree();
			PrintTest pt = new PrintTest();
			pt.printtests(a, copyx);
		}
	}
	
}