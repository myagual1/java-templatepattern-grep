package wordCount.visitors;
import wordCount.util.*; 
import wordCount.dsForStrings.*;

public class PopulateVisitor implements VisitorInterface{
	String st;
	public PopulateVisitor(String s){
		st = s;
	}
	
	public void visit(AvlTree a){
		FileProcessor fp = new FileProcessor();
		fp.openReader(st);
		String[] line;
		while((line = fp.getStringArray()) != null){
			for(int i = 0; i < line.length; i++){
				a.insert(line[i]);
				
			}
		}
		fp.closeReader();
	}
	
}