package wordCount.visitors;
import wordCount.dsForStrings.*;
import wordCount.util.FileProcessor;
import java.lang.Integer;

public class WordCountVisitor implements VisitorInterface{
	String st;
	public WordCountVisitor(String s){
		st = s;
	}
	
	 public int getTotalWords(AvlTree a){
	    	if(a.getroot() == null)return 0;
	    	return getTotalWords(a.getroot());
	    }
	    
	    private int getTotalWords(AvlNode a){
	    	if(a == null)return 0;
	    	return a.getRepcount() + getTotalWords(a.left) + getTotalWords(a.right);
	    }
	    
	    public int getUniques(AvlTree a){
	    	if(a.getroot() == null)return 0;
	    	return getUniques(a.getroot());
	    }
	    
	    private int getUniques(AvlNode a){
	    	if(a == null)return 0;
	    	return 1 + getUniques(a.left) + getUniques(a.right);
	    }
	    
	    public int getChars(AvlTree a){
	    	if(a.getroot() == null)return 0;
	    	return getChars(a.getroot());
	    }
	    
	    private int getChars(AvlNode a){
	    	if(a == null)return 0;
	    	int p1 = 0;
	    	if(a.getRepcount() > 1){
	    		p1 = ((a.getelement()).length()) * a.getRepcount();
	    	}
	    	else{
	    		p1 = (a.getelement()).length();
	    	}
	    	return p1 + getChars(a.left) + getChars(a.right);  	 
	    }
	
	public void visit(AvlTree a){
		FileProcessor fp = new FileProcessor();
		fp.openWriter(st);
		int total = getTotalWords(a);
		int uniquetotal = getUniques(a);
		int chartotal = getChars(a);
		fp.writeLine("Total words: " + total);
		fp.writeLine("Total unique words: " + uniquetotal);
		fp.writeLine("Total char: " + chartotal);
		fp.closeWriter();
	}
	
}